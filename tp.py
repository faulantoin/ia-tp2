import numpy as np
import tensorflow as tf
import datetime

from tensorflow.keras.layers import Input, Dense, Activation, Reshape, Conv1D,Flatten

#Define constants
EPOCHS = 1000
BATCH_SIZE = 40

#Define optimizers, losses and dataset
m_bits=128
k_bits=m_bits # key has same lenght as text
optimizer = tf.optimizers.Adam(learning_rate=0.001)

def custom_loss(out, y):
    sub = tf.math.abs(tf.math.subtract(out, tf.cast(y,'float32')))
    re = tf.math.reduce_sum(sub, axis=1)
    x= tf.math.divide(re, m_bits)
    return x

def bits_metric(out, y):
    out = tf.math.round(out)
    sub = tf.math.subtract(out, tf.cast(y, 'float32'))
    abss = tf.math.abs(sub)
    return (m_bits-tf.math.reduce_sum(abss, axis=1))/m_bits

#Tensorboard var
current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
event_log_dir = 'logs/'+current_time+'/train'
event_summary_writer = tf.summary.create_file_writer(event_log_dir)


# Alice, Bob & eve model. Same structure, except that eve only get the crypted message in the input.
# network structure found in internet, to try to improve our IA. (our custom version wasn't efficent enouth)
def generic_model(length):
    model = tf.keras.Sequential()
    model.add(Input(shape=(length,))) # input : message + key (alice) OR crypted message + key (bob) OR crypted message (eve)
    model.add(Dense(units=length))
    model.add(Reshape((length, 1))) # 256 -> (256,1)
    model.add(Conv1D(1, 2, padding='SAME',  activation=tf.nn.sigmoid))   
    model.add(Conv1D(1, 2, padding='VALID',  activation=tf.nn.sigmoid)) # (256,1) -> (255,1)
    model.add(Conv1D(1, 2, padding='SAME',  activation=tf.nn.sigmoid)) 
    model.add(Conv1D(1, 2, padding='SAME',  activation=tf.nn.tanh))
    model.add(Flatten()) #   (255,1) -> 255
    model.add(Dense(units=m_bits)) # 255 -> 128. We only want the crypted message
    return model

#text & key generation
def generate_datas():
    texts = []
    keys = []
    #for i in range(BATCH_SIZE):
    texts = np.random.randint(0, 2, m_bits*BATCH_SIZE).reshape(BATCH_SIZE, m_bits)
    keys = np.random.randint(0, 2, k_bits*BATCH_SIZE).reshape(BATCH_SIZE, k_bits)
    texts = tf.convert_to_tensor(texts)
    keys = tf.convert_to_tensor(keys)
    return (texts,keys)


# MAIN
if __name__=='__main__':
#model creation
    alice = generic_model(m_bits+k_bits)
    bob = generic_model(m_bits+k_bits)
    eve = generic_model(m_bits)
    print(alice.summary())
    print(bob.summary())

#text & key generation
    (texts,keys) = generate_datas()
#Training
    for epoch in range(EPOCHS):
        mloa = tf.keras.metrics.Mean()
        mlob = tf.keras.metrics.Mean()
        mloe = tf.keras.metrics.Mean()
        mbb = tf.keras.metrics.Mean()
        mev = tf.keras.metrics.Mean()
        with tf.GradientTape(persistent=True) as atape:

            textekey = tf.concat([texts, keys], axis=1) #alice input creation
            aout = alice(textekey, training=True)
            

            bobin = tf.concat([aout, tf.cast(keys, 'float32')], axis=1) # bob input creation

            bout = bob(bobin, training=True)
            eout = eve(aout, training=True)

            #loss
            loe = custom_loss(eout, tf.cast(tf.convert_to_tensor(texts), 'float32'))
            lob = custom_loss(bout, tf.cast(tf.convert_to_tensor(texts), 'float32'))
            loa = lob + (2*(1-loe))

            #compute valids bit for eve & bob
            mbb.update_state(bits_metric(bout, texts))
            mev.update_state(bits_metric(eout, texts))

            #compute mean metric
            mloa.update_state(loa)
            mlob.update_state(lob)
            mloe.update_state(loe)
                
        grads = atape.gradient(loa, alice.trainable_variables)
        optimizer.apply_gradients(zip(grads, alice.trainable_variables))
        grads = atape.gradient(lob, bob.trainable_variables)
        optimizer.apply_gradients(zip(grads, bob.trainable_variables))
        grads = atape.gradient(loe, eve.trainable_variables)
        optimizer.apply_gradients(zip(grads, eve.trainable_variables))
        del atape #avoid memory leak
        with event_summary_writer.as_default():
            tf.summary.scalar('bob-loss', mlob.result().numpy(), step=epoch)
            tf.summary.scalar('eve-loss', mloe.result().numpy(), step=epoch)
            tf.summary.scalar('alice-loss', mloa.result().numpy(), step=epoch)
            tf.summary.scalar('Bob valid bites', mbb.result().numpy(), step=epoch)
            tf.summary.scalar('Eve valid bites', mev.result().numpy(), step=epoch)
        print("Epoch "+str(epoch)+ " - Bob loss : "+str(mlob.result().numpy())+" - Eve loss : "+str(mloe.result().numpy()))
