# ia-tp2
### Antoine Faul, Maxime Graff
## Installation du projet
Après avoir cloné ou téléchargé le projet, il faut installer les dépendances. Pour cela, executer :

`sudo pip3 install -r requirements.txt`

## Lancement du projet
Pour lancer le projet, executer :

`python3 tp.py`

Après l'execution du programme, vous pouvez visualisez les résultats de l'apprentissage en utilisant *Tensorboard*. Pour lancer tensorboard, executez : 
`tensorboard --logdir logs/`

Vous aurez accès aux différentes metrics de notre programme, tel que la loss de alice, bob et eve, ainsi que le % de bits trouvé par eve et bob. On remarque que bob atteint les 100% de réussite à la fin de l'apprentissage, alors qu'eve stagne autour des 50% de réussite, ce qui s'apparante à un résultat obtenu après un tirage aléatoire étant données que les données sont des bits (0 ou 1). On peut donc dire que Alice et Bob arrivent à mettre en place un moyen de communication qu'eve n'arrive pas à décrypter.

## Explications
Nous sommes arrivé à ce résultat en passant par plusieurs étapes : Tout d'abord, Alice et Bob s'échangeaient des messages en clairs, facilement devinable par Eve. Nous avons donc rajouter la réussite de Eve dans la loss de Alice. Mais même avec cela, notre model n'était pas bon. Nous nous sommes donc inspiré des modèles de réseaux existants sur internet, et nous sommes arrivé au résultat actuel.
